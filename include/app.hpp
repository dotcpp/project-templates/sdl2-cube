#ifndef APP_H
#define APP_H

#include <glm/glm.hpp>
#include <glprogram.hpp>
#include <memory>
#include <string>
#include <vector>

class App
{
public:
    App(const std::vector<std::string> &args);
    virtual ~App();

    bool Init();
    int Run();

    void OnInit();
    void OnFrame();
    void OnResize(int width, int height);
    void OnExit();

    template <class T>
    T *GetWindowHandle() const;

protected:
    const std::vector<std::string> &_args;
    glm::mat4 _projection;
    unsigned int _vao, _vbo;
    std::unique_ptr<GlProgram> _program;

    template <class T>
    void SetWindowHandle(T *handle);

    void ClearWindowHandle();

    static float vertices[180];

private:
    void *_windowHandle;
};

#endif // APP_H
